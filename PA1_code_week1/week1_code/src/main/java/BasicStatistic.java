import java.util.ArrayList;

/**
 * very simple implementation of the BasicStatisticInterface
 */
public class BasicStatistic implements BasicStatisticInterface {

    ArrayList<Double> temp;

    public BasicStatistic(){
        temp = new ArrayList<>();
    }

    @Override
    public void addDoubleToData(Double valueToAdd){
        temp.add(valueToAdd);
    }

    @Override
    public void clearData(){
        temp.clear();
    };
	
    @Override
    public int numberOfDataItems(){
        int rv = 0;
        rv = temp.size();
        return rv;
    }

    @Override
    public Double sum(){
        double rv = 0.0;
        for(int i =0;i<temp.size();i++){
            rv += temp.get(i);
        }
        return rv;
    }

}
