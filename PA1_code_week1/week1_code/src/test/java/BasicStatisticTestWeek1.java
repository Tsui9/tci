import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BasicStatisticTestWeek1 {

    @Test
    public void addDoubleToData() {
        //arrange
        BasicStatistic bs = new BasicStatistic();
        //act
        int size = bs.numberOfDataItems();
        //assert
        //the number of data items is 0 when none are present
        Assert.assertEquals(0,size);
        //arrange
        bs.addDoubleToData(2.3);
        //act
        size = bs.numberOfDataItems();
        //assert
        //after adding a Double, the number of items is != 0
        Assert.assertNotEquals(0,size);
    }

    @Test
    //outcome and method name should be including the name
    public void clearData() {
        BasicStatistic bs = new BasicStatistic();
        bs.addDoubleToData(2.3);
        bs.clearData();
        int size = bs.numberOfDataItems();
        Assert.assertEquals(0,size);
    }

    @Test
    public void numberOfDataItems() {
    }

    @Test
    public void sum() {
        BasicStatistic bs = new BasicStatistic();
        bs.addDoubleToData(2.3);
        bs.addDoubleToData(3.2);
        bs.addDoubleToData(0.5);
        double size = bs.sum();
        Assert.assertEquals(6,size,0);
    }
}